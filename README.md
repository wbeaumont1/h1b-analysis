This repo is to create a quick analysis on h1b salaries reported by the department of labor.  The goal is to gather and understand the range of salaries associated with various positions in tech.

----

[Glassdoor](https://www.glassdoor.com/research/h1b-workers/) published an article comparing h1b wages to those reported on their website.  They found that on average h1b workers are paid 2.8% more than wages reported on Glassdoor.  The difference in pay depended on the job and location.

This study will primarily use the h1b data set to compare companies to each other.  It could be interesting to add other job salary databases later.